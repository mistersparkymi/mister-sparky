At Mister Sparky Electrical, we pride ourselves in being the one stop shop for all of your home electrical needs. We understand how important having someone you can depend on to repair or service your homes electrical components is to you, which is why we offer same day service.

Address: 10251 Bergin Rd, Howell, MI 48843, USA

Phone: 517-513-6599

Website: https://mistersparkymi.com